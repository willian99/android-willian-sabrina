package service;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import model.PUB;

public class PublicacoesService extends AsyncTask<Void, Void, PUB>{

    private String pub;

    public PublicacoesService(String pub){

        Log.d("AST", "construtor");

        if (pub != null && pub.length() > 1){
            this.pub = pub;
        } else {
            throw new IllegalArgumentException("Publicação Inválida");
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.i("AST", "onPreExecute");

    }

    @Override
    protected void onPostExecute(PUB pub) {
        super.onPostExecute(pub);
        Log.i("AST", "onPostExecute");

    }

    @Override
    protected PUB doInBackground(Void... voids) {
        Log.i("AST", "doInBackground");

        StringBuilder resposta = new StringBuilder();

        try{
            URL url = new URL("http://servicodados.ibge.gov.br/api/v1/publicacoes/"+ this.pub);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();

            Scanner scanner = new Scanner(url.openStream());

            while (scanner.hasNext()){
                resposta.append(scanner.next());
            }
            Log.d("Teste",resposta.toString());

        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return new Gson().fromJson(resposta.toString(), PUB.class);
        //return null;
    }
}