package br.edu.ifc.araquari.aula.consultacep;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import model.PUB;

public class ResultadoActivity extends AppCompatActivity {

    TextView tvResultado;
    FloatingActionButton fabCompartilhar;
    ListView lvCEP;
    PUB publi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        this.tvResultado = findViewById(R.id.tv_resultado);
        this.fabCompartilhar = findViewById(R.id.fab_resultado_compartilhar);
        this.lvCEP = findViewById(R.id.lv_resultado_cep);

        //obter a intent que iniciou esta activity
        Intent intent = this.getIntent();
        this.publi = (PUB) intent.getSerializableExtra("PUB");
        //this.tvResultado.setText(publi.toString());

        ArrayList dadosEndereco = new ArrayList<String>();
        dadosEndereco.add("Existem " + publi.getCep() + " publicações feitas pelo IBGE sobre esse assunto. \n" );
        //dadosEndereco.add("cidade: \n" + cep.getLocalidade());
        //dadosEndereco.add("estado: \n" + cep.getUf());

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dadosEndereco);
        lvCEP.setAdapter(adapter);


    }

    public void compartilhar(View view) {

        Intent intentCompartilhar = new Intent(Intent.ACTION_SEND);
        intentCompartilhar.setType("text/plain");

        intentCompartilhar.putExtra(Intent.EXTRA_TEXT, this.publi.toString());
        intentCompartilhar.putExtra(Intent.EXTRA_SUBJECT, "compart. PUB");

        startActivity(intentCompartilhar);

    }
}
