package br.edu.ifc.araquari.aula.consultacep;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import model.PUB;
import service.PublicacoesService;

public class ConsultaActivity extends AppCompatActivity {

    EditText edtPub;
    TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        this.edtPub = findViewById(R.id.edt_consulta_pub);
        this.tvResultado = findViewById(R.id.tv_consulta_resultado);

    }

    public boolean isOnline(){
        //TODO OBTER STATUS DA REDE
        return true;
    }

    public void buscarPUB(View view) {

        String pub = edtPub.getText().toString();

        try {

            PUB pubRetorno = new PublicacoesService(pub).execute().get();
            Intent intent = new Intent(ConsultaActivity.this, ResultadoActivity.class);

            intent.putExtra("PUB", pubRetorno);
            this.startActivity(intent);


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            Toast.makeText(ConsultaActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
